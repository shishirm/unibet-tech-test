
using Microsoft.EntityFrameworkCore;
using UnibetApp.Domain.Entities;
using System;

namespace UnibetApp.Infrastructure.Data
{
    public class ApplicationDBContext : DbContext, IDisposable
    {
        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options) : base(options)
        {
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    //this.Database.CloseConnection();
                    base.Dispose();
                }
                disposedValue = true;
            }
        }
        void IDisposable.Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
          modelBuilder.Entity<Currency>(b =>
          {
              b.ToTable("Currency");
              b.HasKey(u => u.Id);
          });

          modelBuilder.Entity<CurrencyExchange>(b =>
          {
              b.ToTable("CurrencyExchange");
              b.HasKey(u => u.Id);
          });
          base.OnModelCreating(modelBuilder);
        }
    }
}