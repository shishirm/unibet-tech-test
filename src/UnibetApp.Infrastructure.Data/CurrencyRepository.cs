using UnibetApp.Domain.Entities;
using UnibetApp.Domain.Interfaces;

namespace UnibetApp.Infrastructure.Data
{
    public class CurrencyRepository : Repository<Currency>, ICurrencyRepository
    {
        public CurrencyRepository(ApplicationDBContext dataContext) : base(dataContext)
        {
        }
    }
}