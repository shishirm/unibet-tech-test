﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;
using UnibetApp.Infrastructure.Data;

namespace UnibetApp.Web.App.Data.Migrations
{
    [DbContext(typeof(ApplicationDBContext))]
    [Migration("00000000000000_CreateIdentitySchema")]
    public class CreateIdentitySchema : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Currency",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Code = table.Column<string>(maxLength: 10, nullable: false),
                    RefreshedEpoch = table.Column<string>(maxLength: 12, nullable: false),
                    RefreshedDate =  table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Currency", x => x.Id);
                });

           migrationBuilder.CreateTable(
                name: "CurrencyExchange",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CurrencyId = table.Column<int>(nullable: false),
                    Code = table.Column<string>(maxLength: 10, nullable: false),                    
                    ExchnageRate =  table.Column<double>(nullable:false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CurrencyExchange", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CurrencyExchange_Currency_CurrencyId",
                        column: x => x.CurrencyId,
                        principalTable: "Currency",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });     
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_CurrencyExchange_Currency_CurrencyId",  table: "CurrencyExchange");
            migrationBuilder.DropTable(name: "CurrencyExchange");
            migrationBuilder.DropTable(name: "Currency");
        }
    }
}