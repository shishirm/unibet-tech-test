using UnibetApp.Domain.Entities;
using UnibetApp.Domain.Interfaces;

namespace UnibetApp.Infrastructure.Data
{
    public class CurrencyExchangeRepository : Repository<CurrencyExchange>, ICurrencyExchangeRepository
    {
        public CurrencyExchangeRepository(ApplicationDBContext dataContext) : base(dataContext)
        {
        }
    }
}