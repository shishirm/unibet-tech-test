using UnibetApp.Domain.Entities;

namespace UnibetApp.Domain.Interfaces
{
    public interface ICurrencyExchangeRepository : IRepository<CurrencyExchange>
    {
    }
}