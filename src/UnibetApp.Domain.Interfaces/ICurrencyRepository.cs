using UnibetApp.Domain.Entities;

namespace UnibetApp.Domain.Interfaces
{
    public interface ICurrencyRepository : IRepository<Currency>
    {
    }
}