using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RestSharp;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net;
using System;
using UnibetApp.Common;
using UnibetApp.Domain.Interfaces;
using UnibetApp.Services.Interface;

namespace UnibetApp.Services.Business
{
    public class RateRefresherService : IRateRefresherService
    {
        private readonly ILogger<RateRefresherService> _logger;
        private IOptions<AppSettings> _options;
        private ICurrencyRepository _currencyRepository;
        private ICurrencyExchangeRepository _currencyExchangeRepository;
        private IRestClient _restClient;

        public RateRefresherService(ILogger<RateRefresherService> logger,
                IOptions<AppSettings> options,
                ICurrencyRepository currencyRepository,
                ICurrencyExchangeRepository currencyExchangeRepository,
                IRestClient restClient)
        {
            _logger = logger;
            _options = options;
            _currencyRepository = currencyRepository;
            _currencyExchangeRepository = currencyExchangeRepository;
            _restClient = restClient;
        }

        private void SetBaseURL(string baseCurrency)
        {
            var allCurrencies = this._options.Value.BusinessCurrencies.Split(',');
            var apiKey = this._options.Value.FixerAPIKey;
            var baseURL = this._options.Value.FixerURL;
            var symbolCurrencies = string.Join(",", allCurrencies);
            var targetURL = $"{baseURL}{apiKey}&base={baseCurrency}&symbols={symbolCurrencies}";
            _restClient.BaseUrl = new Uri(targetURL);

        }

        private int CreateBaseCurrency(string baseCurrency, FixerCurrencyResponse responseContent)
        {
            var newCurrency = new Domain.Entities.Currency();
            newCurrency.Code = baseCurrency;
            newCurrency.RefreshedEpoch = responseContent.Timestamp;
            newCurrency.RefreshedDate = DateTime.UtcNow;
            _currencyRepository.Create(newCurrency);
            return newCurrency.Id;
        }

        private int UpdateBaseCurrency(Domain.Entities.Currency existingCurrency, FixerCurrencyResponse responseContent)
        {
            existingCurrency.RefreshedDate = DateTime.UtcNow;
            existingCurrency.RefreshedEpoch = responseContent.Timestamp;
            _currencyRepository.Update(existingCurrency);
            return existingCurrency.Id;
        }

        public RefreshResult RefreshCurrenciesRate()
        {
            var result = new RefreshResult();
            foreach (var baseCurrency in this._options.Value.BusinessCurrencies.Split(','))
            {
                try
                {
                    var request = new RestRequest();
                    request.Method = Method.GET;
                    this.SetBaseURL(baseCurrency);

                    var response = this._restClient.Execute(request);
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responseContent = JsonConvert.DeserializeObject<FixerCurrencyResponse>(response.Content);
                        if (responseContent.Success && responseContent.Base == baseCurrency)
                        {
                            var existingCurrency = _currencyRepository.GetAll().Where(x => x.Code == baseCurrency).FirstOrDefault();
                            int baseCurrencyId = 0;

                            if (existingCurrency == null)
                            {
                                baseCurrencyId = this.CreateBaseCurrency(baseCurrency, responseContent);
                                result.NumberOfBaseCurrencyInserted = result.NumberOfBaseCurrencyInserted + 1;
                            }

                            else
                            {
                                baseCurrencyId = this.UpdateBaseCurrency(existingCurrency, responseContent);
                                result.NumberOfBaseCurrencyUpdated = result.NumberOfBaseCurrencyUpdated + 1;
                                var rates = _currencyExchangeRepository.GetAll().Where(x => x.CurrencyId == baseCurrencyId).ToList();
                                //Delete existing rates  for a given base currency
                                if (rates != null && rates.Count > 0)
                                {
                                    _currencyExchangeRepository.Deletecollection(rates);
                                    result.NumberOfExchangeRatesDeleted = result.NumberOfExchangeRatesDeleted + rates.Count;
                                }
                            }

                            var newRates = new List<Domain.Entities.CurrencyExchange>();
                            foreach (KeyValuePair<string, double> entry in responseContent.Rates)
                            {
                                newRates.Add(new Domain.Entities.CurrencyExchange
                                {
                                    CurrencyId = baseCurrencyId,
                                    Code = entry.Key,
                                    ExchnageRate = entry.Value
                                });
                            }

                            //insert latest rates  for a given base currency
                            if (newRates.Count > 0)
                            {
                                _currencyExchangeRepository.CreateCollections(newRates);
                                result.NumberOfExchangeRatesInserted = result.NumberOfExchangeRatesInserted + newRates.Count;
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    _logger.LogError(ex.StackTrace);
                }
            }
            return result;
        }
    }
}