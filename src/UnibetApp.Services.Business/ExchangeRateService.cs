using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System;
using UnibetApp.Common;
using UnibetApp.Domain.Interfaces;
using UnibetApp.Services.Interface;
using Newtonsoft.Json;

namespace UnibetApp.Services.Business
{
    public class ExchangeRateService : IExchangeRateService
    {
        private readonly ILogger<ExchangeRateService> _logger;
        private ICurrencyRepository _currencyRepository;
        private ICurrencyExchangeRepository _currencyExchangeRepository;

        public ExchangeRateService(ILogger<ExchangeRateService> logger,
                ICurrencyRepository currencyRepository,
                ICurrencyExchangeRepository currencyExchangeRepository)
        {
            _logger = logger;
            _currencyRepository = currencyRepository;
            _currencyExchangeRepository = currencyExchangeRepository;
        }

        public ExchangeRateDto GetExchangeRate(string baseCurrency, string targetCurrency)
        {
            _logger.LogInformation($"GetExchangeRate Called for BaseCurrency {baseCurrency} and Target Currency {targetCurrency}");
            try
            {
                return (from currency in _currencyRepository.GetAll()
                        join currencyExchange in _currencyExchangeRepository.GetAll() on currency.Id equals currencyExchange.CurrencyId
                        where (string.Equals(currency.Code.ToLower(), baseCurrency.ToLower())
                            && string.Equals(currencyExchange.Code.ToLower(), targetCurrency.ToLower()))
                        select new ExchangeRateDto
                        {
                            BaseCurrency = baseCurrency,
                            TargetCurrency = targetCurrency,
                            ExchangeRate = currencyExchange.ExchnageRate,
                            Timestamp = currency.RefreshedDate

                        }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                throw;
            }
        }
    }
}