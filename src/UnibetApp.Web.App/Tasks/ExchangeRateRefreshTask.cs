using AutoMapper;
using UnibetApp.Services.Interface;
using UnibetApp.Web.App.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System;
using Microsoft.Extensions.DependencyInjection;


namespace UnibetApp.Web.App.Tasks
{
    public interface IExchangeRateRefreshTask
    {
        void ExecuteTask();
    }

    public class ExchangeRateRefreshTask : IExchangeRateRefreshTask
    {
        private readonly ILogger<ExchangeRateRefreshTask> _logger;
        private readonly IServiceProvider _serviceProvider;
        private IRateRefresherService _rateRefresherService;

        public ExchangeRateRefreshTask(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            // _logger = serviceProvider.GetRequiredService<ILogger<ExchangeRateRefreshTask>>();
        }

        public void ExecuteTask()
        {
            if (_rateRefresherService == null)
                _rateRefresherService = _serviceProvider.GetRequiredService<IRateRefresherService>();

            Console.WriteLine("Execute the task!");
            _rateRefresherService.RefreshCurrenciesRate();
            // _logger.LogInformation("Execute the task!");
        }
    }
}
