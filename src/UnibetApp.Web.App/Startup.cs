using UnibetApp.Infrastructure.DependencyResolver;
using UnibetApp.Web.App.Middlewares;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using NLog.Extensions.Logging;
using Newtonsoft.Json;
using System.IO;
using UnibetApp.Common;
using Hangfire;
using System;
using UnibetApp.Web.App.Tasks;

namespace UnibetApp.Web.App
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }
        public IConfigurationRoot Configuration { get; }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            Console.WriteLine("ConfigureServices");
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));
            services.AddOptions();
            var serviceProvider = services.BuildServiceProvider();

            services.AddMvc().AddJsonOptions(opt =>
              {
                  opt.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
                  opt.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
              });
            DependencyInstaller.InjectDependencies(services, this.Configuration);
            services.AddTransient<IExchangeRateRefreshTask, ExchangeRateRefreshTask>();
        }



        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            Console.WriteLine("Configure");

            this.SetupLoggingProvider(loggerFactory);
            app.Use(new UnhandledExceptionMiddleware().Process);

            if (env.IsDevelopment())
                app.UseBrowserLink();
            else
                app.UseExceptionHandler("/Home/Error");



            if (!env.IsDevelopment())
            {
                app.UseHsts(options => options.MaxAge(days: 30));
            }

            app.UseStaticFiles();

            if (!env.IsDevelopment())
            {
                //Configuring X-Frame-Options
                app.UseXfo(options => options.SameOrigin()); //need to check if it will work from blackbook . to stop click jacking
                app.UseXXssProtection(options => options.EnabledWithBlockMode());
            }

            app.UseAuthentication();
            app.UseMvc();
            app.UseHangfireServer();
            AutoMapperBootStrapper.Bootstrap();

            // do at starting
            BackgroundJob.Schedule(() => new ExchangeRateRefreshTask(app.ApplicationServices).ExecuteTask(), TimeSpan.FromSeconds(10));
            // Schedule for every hour
            RecurringJob.AddOrUpdate(() => new ExchangeRateRefreshTask(app.ApplicationServices).ExecuteTask(), Cron.HourInterval(1));
        }
        private void SetupLoggingProvider(ILoggerFactory factory)
        {
            factory.AddDebug();
            factory.AddConsole();
            factory.AddNLog(new NLogProviderOptions { CaptureMessageTemplates = true, CaptureMessageProperties = true });
            factory.ConfigureNLog("nlog.config");
        }

        public void ExecuteTask(IApplicationBuilder app)
        {
            var exchangeRateRefreshTask = new ExchangeRateRefreshTask(app.ApplicationServices);
            exchangeRateRefreshTask.ExecuteTask();
        }
    }
}
