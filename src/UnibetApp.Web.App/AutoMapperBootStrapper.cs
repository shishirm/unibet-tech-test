using UnibetApp.Web.App.Models;

namespace UnibetApp.Web.App
{
    public class AutoMapperBootStrapper
    {
        public static void Bootstrap() => ConfigurePageMappings();

        private static void ConfigurePageMappings()
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
            });
        }
    }
}
