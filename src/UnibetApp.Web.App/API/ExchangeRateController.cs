using AutoMapper;
using UnibetApp.Services.Interface;
using UnibetApp.Web.App.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace UnibetApp.Web.App.Controllers
{
    [Route("api/[controller]")]
    public class ExchangeRateController : Controller
    {
        private readonly ILogger<ExchangeRateController> _logger;
        private readonly IExchangeRateService _exchangeRateService;

        public ExchangeRateController(IExchangeRateService exchangeRateService,
        ILogger<ExchangeRateController> logger)
        {
            _exchangeRateService = exchangeRateService;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult GetCurrenices(string baseCurrency, string targetCurrency)
        {
            _logger.LogInformation($"GetCurrenices Called for BaseCurrency {baseCurrency} and Target Currency {targetCurrency}");
            try
            {
                var exchangeRateResult = _exchangeRateService.GetExchangeRate(baseCurrency, targetCurrency);

                if (exchangeRateResult == null)
                    return NotFound("Currency not found");

                return Ok(exchangeRateResult);
            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex.Message);
                _logger.LogError(ex.StackTrace);
                return StatusCode(500);
            }
        }
    }
}