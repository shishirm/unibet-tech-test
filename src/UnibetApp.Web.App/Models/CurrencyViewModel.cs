using System;
using System.ComponentModel.DataAnnotations;

namespace UnibetApp.Web.App.Models
{
    public class CurrencyViewModel
    {
        public int Id { get; set; }

        public string Code { get; set; }
        public string RefreshedEpoch { get; set; }
        public DateTime RefreshedDate { get; set; }        
    }
    
    public class CurrencyExchangeViewModel     
    {        
        public int CurrencyId { get; set; }
        public string Code { get; set; }
        public double ExchnageRate { get; set; }        
    }
}
