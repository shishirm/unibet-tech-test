using System;

namespace UnibetApp.Common
{
    public class RefreshResult
    {
        public int NumberOfBaseCurrencyInserted {get;set;}
        public int NumberOfBaseCurrencyUpdated{get;set;}
        public int NumberOfExchangeRatesDeleted{get;set;}
        public int NumberOfExchangeRatesInserted{get;set;}

    }
}