using System;

namespace UnibetApp.Common
{
    public class ExchangeRateDto
    {
        public string BaseCurrency { get; set; }
        public string TargetCurrency { get; set; }
        public double ExchangeRate { get; set; }
        public DateTime Timestamp { get; set; }

        public override bool Equals(object obj)
        {
            var item = obj as ExchangeRateDto;
            return ((item.BaseCurrency == this.BaseCurrency) &&
                        (item.TargetCurrency == this.TargetCurrency) &&
                        (item.ExchangeRate == this.ExchangeRate));
        }

        public override int GetHashCode()
        {
            var stringValue = $"{this.BaseCurrency}-{this.TargetCurrency}-{this.ExchangeRate.ToString()}";
            return this.BaseCurrency.GetHashCode();
        }
    }
}