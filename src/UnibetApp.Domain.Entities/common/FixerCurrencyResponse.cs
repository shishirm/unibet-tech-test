using System;
using System.Collections.Generic;

namespace UnibetApp.Common
{
    public class FixerCurrencyResponse
    {
        public bool Success { get; set; }
        public string Timestamp { get; set; }
        public string Base { get; set; }
        public string Date { get; set; }
        public Dictionary<string, double> Rates { get; set; }
    }
}