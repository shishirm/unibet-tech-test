namespace UnibetApp.Common
{
    public class AppSettings
    {
        public string FixerURL { get; set; }
        public string FixerAPIKey { get; set; }
        public string BusinessCurrencies { get; set; }
    }
}