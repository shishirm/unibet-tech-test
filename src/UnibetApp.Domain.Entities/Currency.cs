using System;
using System.Collections.Generic;
namespace UnibetApp.Domain.Entities
{
    public class Currency : BaseEntity
    {
        public string Code { get; set; }
        public string RefreshedEpoch { get; set; }
        public DateTime RefreshedDate { get; set; }
    }
}
