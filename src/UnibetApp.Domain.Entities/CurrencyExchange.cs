using System;
using System.Collections.Generic;
namespace UnibetApp.Domain.Entities
{
    public class CurrencyExchange : BaseEntity
    {        
        public int CurrencyId { get; set; }
        public string Code { get; set; }
        public double ExchnageRate { get; set; }        
    }
}
