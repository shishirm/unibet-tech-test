using System.Collections.Generic;
using System;
using UnibetApp.Common;

namespace UnibetApp.Services.Interface
{
    public interface IExchangeRateService
    {
        ExchangeRateDto GetExchangeRate(string basecurrency, string targetCurrency);
    }
}
