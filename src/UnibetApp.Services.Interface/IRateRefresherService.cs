using System.Collections.Generic;
using System;
using UnibetApp.Common;

namespace UnibetApp.Services.Interface
{
    public interface IRateRefresherService
    {
        RefreshResult RefreshCurrenciesRate();
    }
}