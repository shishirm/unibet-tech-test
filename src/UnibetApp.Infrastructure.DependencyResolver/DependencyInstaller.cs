using Hangfire.MemoryStorage;
using Hangfire;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using UnibetApp.Domain.Interfaces;
using UnibetApp.Infrastructure.Data;
using UnibetApp.Services.Business;
using UnibetApp.Services.Interface;
using RestSharp;

namespace UnibetApp.Infrastructure.DependencyResolver
{
    public class DependencyInstaller
    {
        public static void InjectDependencies(IServiceCollection services, IConfiguration configuration)
        {
            InjectDependenciesForDAL(services, configuration);
            services.AddTransient<IRestRequest, RestRequest>();
            services.AddTransient<IRestClient, RestClient>();
            services.AddTransient<IExchangeRateService, ExchangeRateService>();
            services.AddTransient<IRateRefresherService, RateRefresherService>();
        }

        private static void InjectDependenciesForDAL(IServiceCollection services, IConfiguration configuration)
        {
            var connString = configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<ApplicationDBContext>(options =>
              {
                  options.UseSqlServer(connString, opt =>
                   {
                   });
              });

            services.AddHangfire(hf => hf.UseMemoryStorage()); // schedule jobs in memory
            services.AddTransient<ICurrencyRepository, CurrencyRepository>();
            services.AddTransient<ICurrencyExchangeRepository, CurrencyExchangeRepository>();
        }
    }
}