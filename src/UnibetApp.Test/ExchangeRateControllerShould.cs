using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Newtonsoft.Json;
using System;
using UnibetApp.Common;
using UnibetApp.Services.Business;
using UnibetApp.Services.Interface;
using UnibetApp.Web.App.Controllers;
using UnibetApp.Web.App.Models;
using Xunit;

namespace UnibetApp.Test
{
    public class ExchangeRateControllerShould
    {
        [Fact]
        public void TestException()
        {
            var mockService = new Mock<IExchangeRateService>();
            mockService.Setup(x => x.GetExchangeRate(It.IsAny<string>(), It.IsAny<string>())).Throws(new System.Exception());
            var logger = new Mock<ILogger<ExchangeRateController>>();
            var controller = new ExchangeRateController(mockService.Object, logger.Object);
            var result = controller.GetCurrenices("EUR", "AUD");
            Assert.Equal(500, (result as StatusCodeResult).StatusCode);
        }

        [Fact]
        public void TesCurrenciesNotFound()
        {
            var mockService = new Mock<IExchangeRateService>();
            mockService.Setup(x => x.GetExchangeRate(It.IsAny<string>(), It.IsAny<string>())).Returns<ExchangeRateDto>(null);
            var logger = new Mock<ILogger<ExchangeRateController>>();
            var controller = new ExchangeRateController(mockService.Object, logger.Object);
            var result = controller.GetCurrenices("INR", "AUD") as NotFoundObjectResult;
            Assert.Equal(404, result.StatusCode);
        }

        [Fact]
        public void TestSuccess()
        {
            var currentTime =   DateTime.UtcNow;

            var mockService = new Mock<IExchangeRateService>();
            var expectedResult = new ExchangeRateDto
            {
                BaseCurrency = "EUR",
                TargetCurrency = "AUD",
                ExchangeRate = 1.582125,
                Timestamp = currentTime

            };

            mockService.Setup(x => x.GetExchangeRate(It.IsAny<string>(), It.IsAny<string>())).Returns(expectedResult);
            var logger = new Mock<ILogger<ExchangeRateController>>();
            var controller = new ExchangeRateController(mockService.Object, logger.Object);
            var result = controller.GetCurrenices("EUR", "AUD") as OkObjectResult;
            var actualResult = result.Value as ExchangeRateDto;
            Assert.Equal(expectedResult, actualResult);
        }
    }
}