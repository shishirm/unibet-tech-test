using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using RestSharp;
using System.Linq;
using System;
using UnibetApp.Common;
using UnibetApp.Domain.Entities;
using UnibetApp.Domain.Interfaces;
using UnibetApp.Services.Business;
using Xunit;
using System.Net;

namespace UnibetApp.Test
{
    public class RateRefresherServiceShould
    {
        [Fact]
        public void TesBaseCurrenciesInsert()
        {
            // We are Testing Insert flow of Base 
            var serviceProvider = Intitialize();

            var currentTime = DateTime.UtcNow;
            var currencies = new Currency[] { new Currency { Id = 1, Code = "USD", RefreshedEpoch = "1530835747", RefreshedDate = currentTime } };
            var eurCurrency = new Currency { Id = 0, Code = "EUR", RefreshedEpoch = "1530835747", RefreshedDate = currentTime };

            var rates = this.GetRates(0);
            // setting up  CurrencyRepository
            var mockCurrencyRepository = new Mock<ICurrencyRepository>();
            mockCurrencyRepository.Setup(c => c.GetAll()).Returns(currencies.AsQueryable); // collection will return USD not UER
            mockCurrencyRepository.Setup(c => c.Create(eurCurrency)); // collection will return USD not UER

            // setting up  CurrencyExchangeRepository
            var mockCurrencyExchangeRepository = this.GetMockCurrencyExchangeRepository(rates);

            var mockResponse = this.GetMockRestResponse();
            // setting up  RestRequest
            var mockRestClient = GetMockRestClient(mockResponse.Object);

            // get everything required for service
            var appSettings = serviceProvider.GetRequiredService<IOptions<AppSettings>>();

            var serviceInstance = new RateRefresherService(this.GetLogger(),
                                                            appSettings,
                                                            mockCurrencyRepository.Object,
                                                            mockCurrencyExchangeRepository.Object,
                                                            mockRestClient.Object
                                                          );

            var result = serviceInstance.RefreshCurrenciesRate();

            Assert.True(result.NumberOfBaseCurrencyInserted == 1
            && result.NumberOfBaseCurrencyUpdated == 0
            && result.NumberOfExchangeRatesDeleted == 0
            && result.NumberOfExchangeRatesInserted == 5);
        }

        [Fact]
        public void TesBaseCurrenciesUpdate()
        {
            // We are Testing Insert flow of Base 
            var serviceProvider = Intitialize();

            var currentTime = DateTime.UtcNow;
            var currencies = new Currency[] { new Currency { Id = 1, Code = "EUR", RefreshedEpoch = "1530835747", RefreshedDate = currentTime } };

            var rates = this.GetRates(1);
            // setting up  CurrencyRepository
            var mockCurrencyRepository = new Mock<ICurrencyRepository>();
            mockCurrencyRepository.Setup(c => c.GetAll()).Returns(currencies.AsQueryable); // collection will return USD not UER
            mockCurrencyRepository.Setup(c => c.Update(currencies[0])); // collection will return USD not UER

            // setting up  CurrencyExchangeRepository
            var mockCurrencyExchangeRepository = this.GetMockCurrencyExchangeRepository(rates);

            var mockResponse = this.GetMockRestResponse();
            // setting up  RestRequest

            var mockRestClient = GetMockRestClient( mockResponse.Object);

            // get everything required for service
            var appSettings = serviceProvider.GetRequiredService<IOptions<AppSettings>>();

            var serviceInstance = new RateRefresherService(this.GetLogger(),
                                                            appSettings,
                                                            mockCurrencyRepository.Object,
                                                            mockCurrencyExchangeRepository.Object,
                                                            mockRestClient.Object
                                                          );
            var result = serviceInstance.RefreshCurrenciesRate();

            Assert.True(result.NumberOfBaseCurrencyInserted == 0
            && result.NumberOfBaseCurrencyUpdated == 1
            && result.NumberOfExchangeRatesDeleted == 5
            && result.NumberOfExchangeRatesInserted == 5);
        }

        private CurrencyExchange[] GetRates(int baseCurrenyId) =>
             new CurrencyExchange[] {
                new CurrencyExchange {  Id = 1, CurrencyId = baseCurrenyId,  Code = "USD", ExchnageRate = 1.169531},
                new CurrencyExchange {  Id = 2, CurrencyId = baseCurrenyId,  Code = "SEK", ExchnageRate = 10.253469},
                new CurrencyExchange { Id = 3, CurrencyId = baseCurrenyId,  Code = "AUD", ExchnageRate =1.582613},
                new CurrencyExchange { Id = 4, CurrencyId = baseCurrenyId,  Code = "EUR", ExchnageRate = 1},
                new CurrencyExchange { Id = 5, CurrencyId =baseCurrenyId,  Code = "USGBPD", ExchnageRate =0.884622},
             };
        private ILogger<RateRefresherService> GetLogger() =>
            (new Mock<ILogger<RateRefresherService>>()).Object;

        private Mock<ICurrencyExchangeRepository> GetMockCurrencyExchangeRepository(CurrencyExchange[] rates)
        {
            var mockCurrencyExchangeRepository = new Mock<ICurrencyExchangeRepository>();
            mockCurrencyExchangeRepository.Setup(c => c.GetAll()).Returns(rates.AsQueryable); // get all should return empty
            mockCurrencyExchangeRepository.Setup(c => c.Deletecollection(rates));
            mockCurrencyExchangeRepository.Setup(c => c.CreateCollections(rates));
            return mockCurrencyExchangeRepository;
        }

        private Mock<IRestClient> GetMockRestClient( IRestResponse response)
        {
            var mockRestClient = new Mock<IRestClient>();
            mockRestClient.Setup(x => x.Execute(It.IsAny<IRestRequest>())).Returns(response);
            return mockRestClient;
        }

        private Mock<IRestResponse> GetMockRestResponse()
        {
            // setting up  RestResponse
            var mockResponse = new Mock<IRestResponse>();
            mockResponse.SetupProperty(x => x.StatusCode, HttpStatusCode.OK);
            mockResponse.SetupProperty(x => x.Content, "{\"success\":true,\"timestamp\":1530848947,\"base\":\"EUR\",\"date\":\"2018-07-06\",\"rates\":{\"EUR\":1,\"AUD\":1.58311,\"SEK\":10.256863,\"USD\":1.169043,\"GBP\":0.884486}}");
            return mockResponse;
        }

        public static IServiceProvider Intitialize()
        {
            var services = new ServiceCollection();
            var builder = new ConfigurationBuilder()
                 .AddJsonFile("appsettings.json");

            var configuration = builder.Build();
            services.Configure<AppSettings>(configuration.GetSection("AppSettings"));
            services.AddOptions();
            return services.BuildServiceProvider();
        }
    }
}