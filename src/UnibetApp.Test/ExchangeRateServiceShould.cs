using Microsoft.Extensions.Logging;
using Moq;
using System.Linq;
using System;
using UnibetApp.Common;
using UnibetApp.Domain.Entities;
using UnibetApp.Domain.Interfaces;
using UnibetApp.Services.Business;
using Xunit;
using Newtonsoft.Json;

namespace UnibetApp.Test
{
    public class ExchangeRateServiceShould
    {
        [Fact]
        public void TesCurrenciesNotFound()
        {
            var currentTime = DateTime.UtcNow;
            var currencies = new Currency[] { new Currency { Id = 1, Code = "EUR", RefreshedEpoch = "1530835747", RefreshedDate = currentTime } };
            var currencyExchanges = new CurrencyExchange[] {
                new CurrencyExchange { Id = 1, CurrencyId = 1,  Code = "USD", ExchnageRate = 1.169531},
                new CurrencyExchange { Id = 2, CurrencyId = 1,  Code = "SEK", ExchnageRate = 10.253469},
                new CurrencyExchange { Id = 3, CurrencyId = 1,  Code = "AUD", ExchnageRate =1.582613},
                new CurrencyExchange { Id = 4, CurrencyId = 1,  Code = "EUR", ExchnageRate = 1},
                new CurrencyExchange { Id = 5, CurrencyId = 1,  Code = "USGBPD", ExchnageRate =0.884622},
             };

            var logger = new Mock<ILogger<ExchangeRateService>>();
            var mockCurrencyRepository = new Mock<ICurrencyRepository>();
            mockCurrencyRepository.Setup(c => c.GetAll()).Returns(currencies.AsQueryable);
            var mockCurrencyExchangeRepository = new Mock<ICurrencyExchangeRepository>();
            mockCurrencyExchangeRepository.Setup(c => c.GetAll()).Returns(currencyExchanges.AsQueryable);
            var serviceInstance = new ExchangeRateService(logger.Object,
            mockCurrencyRepository.Object,
            mockCurrencyExchangeRepository.Object);
            var actualResult = serviceInstance.GetExchangeRate("EUR", "INR");
            Assert.Null(actualResult);
        }

        [Fact]
        public void TestSuccess()
        {
            var currentTime = DateTime.UtcNow;
            var expectedResult = new ExchangeRateDto
            {
                BaseCurrency = "EUR",
                TargetCurrency = "AUD",
                ExchangeRate = 1.582613,
                Timestamp = currentTime

            };

            var currencies = new Currency[] { new Currency { Id = 1, Code = "EUR", RefreshedEpoch = "1530835747", RefreshedDate = currentTime } };
            var currencyExchanges = new CurrencyExchange[] {
                new CurrencyExchange { Id = 1, CurrencyId = 1,  Code = "USD", ExchnageRate = 1.169531},
                new CurrencyExchange { Id = 2, CurrencyId = 1,  Code = "SEK", ExchnageRate = 10.253469},
                new CurrencyExchange { Id = 3, CurrencyId = 1,  Code = "AUD", ExchnageRate =1.582613},
                new CurrencyExchange { Id = 4, CurrencyId = 1,  Code = "EUR", ExchnageRate = 1},
                new CurrencyExchange { Id = 5, CurrencyId = 1,  Code = "USGBPD", ExchnageRate =0.884622},
             };

            var logger = new Mock<ILogger<ExchangeRateService>>();
            var mockCurrencyRepository = new Mock<ICurrencyRepository>();
            mockCurrencyRepository.Setup(c => c.GetAll()).Returns(currencies.AsQueryable);
            var mockCurrencyExchangeRepository = new Mock<ICurrencyExchangeRepository>();
            mockCurrencyExchangeRepository.Setup(c => c.GetAll()).Returns(currencyExchanges.AsQueryable);

            var serviceInstance = new ExchangeRateService(logger.Object,
            mockCurrencyRepository.Object,
            mockCurrencyExchangeRepository.Object);
            var actualResult = serviceInstance.GetExchangeRate("EUR", "AUD");
            Assert.Equal(expectedResult, actualResult);
        }
    }
}