# Exchange Rate Service

In order for a Bookmaker to see their liabilities in a common currency, An exchange rate microservice is required. This exchange rate service will be responsible for collection and delivery of exchange rate values to internal clients. 

This is sample project for code challenge ...

It uses dotnet core 2.0 ( https://github.com/dotnet/core) 


#### Prerequisite ####
Install Dotnet core 2.0 SDK (https://www.microsoft.com/net/download/windows)

#### To Build the Project ####
 - Clone the App from bitbucket;
 - Go to Src folder Run dotnet restore;
 - And then run dotnet build

#### To Setup database ####
 - Go to src\UnibetApp.Web.App;
 - Run dotnet ef database update on command prompt;

#### To run test cases ####
 - Got to src\UnibetApp.Test;
 - run dotnet test;

#### To Run the Project ####
 - Go to src\UnibetApp.Web.App;
 - run dotnet run;
 - application will be hosted at  http://localhost:5000;
